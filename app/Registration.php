<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $fillable=['fst_name','lst_name','Email','password','confirm_password'];
}
