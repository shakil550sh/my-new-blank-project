<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admissions extends Model
{
    protected $fillable=['departments_id','std_name','mobile','address'];

    public function departments()
    {
    	return $this->belongsTo('App\Departments');
    }
}
