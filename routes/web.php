<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/home','ShakilController@Home');
Route::get('/Products','ShakilController@Product');

Route::get('/Registration/create','ShakilController@create');
Route::post('/Registration','ShakilController@store');

Route::get('/Aboutus','ShakilController@Aboutus');



// frome shahin sirs prectice
Route::get('/shahin','FrontendController@View');


//department

// Route::get('/departments/create','DepartmentController@create');

// Route::post('/departments','DepartmentController@store');


// Route::get('/departments','DepartmentController@index');
// // Route::get('/department-delete/{id}','DepartmentController@destroy');
// Route::delete('/departments/{id}','DepartmentController@destroy');
// Route::get('/departments/{id}/edit','DepartmentController@edit');


// Route::patch('/departments/{id}','DepartmentController@update');


Route::resource('departments','DepartmentController');




//Student


Route::resource('/students','StudentController');

// Route::get('/students/create','StudentController@create');
// Route::post('/students','StudentController@store');

Route::get('/admissions/create','AdmissionController@create');

Route::post('/admissions','AdmissionController@store');
Route::get('/admissions','AdmissionController@index');

Route::get('/admissions/{id}/edit','AdmissionController@edit');
Route::patch('/admissions/{id}','AdmissionController@update');
Route::delete('/admissions/{id}','AdmissionController@destroy');






