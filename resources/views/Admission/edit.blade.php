@extends('layouts.frontend.master')

@section('title','Admission Update')

@section('content')


@include('messages.message')

	{!! Form::open(['url' => '/admissions/'.$data->id, 'method'=>'PATCH', 'class'=>'form-group','enctype'=>'multipart/form-data']) !!}

 
	
	<select name="departments_id">
		 
		
		@foreach($departments as $datad)
		
          @if($datad->id==$data->departments_id)
          <option value="{{$datad->id}}" selected="">{{$datad->dpt_name}}</option>
          @else
		<option value="{{$datad->id}}">{{$datad->dpt_name}}</option>
		@endif

		@endforeach
	</select>

	<input type="text" name="std_name" placeholder="std name"  value="{{$data->std_name}}">
	<input type="text" name="mobile" placeholder="mobile" value="{{$data->mobile}}">

	<textarea name="address" placeholder="address">{{$data->address}}</textarea>

	<input type="submit" value="Update">
 

{{Form::close()}}

@endsection



