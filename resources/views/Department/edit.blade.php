<!DOCTYPE html>
<html>
<head>
	<title>Department Edit</title>
</head>
<body>

	<h2>Department Edit</h2>   <h3> <a href="/departments"> View Department</a> </h3>
 
 @if ($errors->any())
     @foreach ($errors->all() as $error)
         <div>{{$error}}</div>
     @endforeach
 @endif
 
@if(session()->has('success'))
    <div class="alert alert-success" style="color: green;font-weight: bold;">
        {{ session()->get('success') }}
    </div>
@endif

 

		{!! Form::open(['url' => '/departments/'.$data->id, 'method'=>'PATCH', 'class'=>'form-group','enctype'=>'multipart/form-data']) !!}

		<input type="text" name="dpt_name" placeholder="department name" value="{{$data->dpt_name}}">
		<input type="text" name="dpt_code" placeholder="department code"  value="{{$data->dpt_code}}">
		<input type="submit" value="Update">
		
   {!!  Form::close() !!}

<br>
<br>
<br>
	 

</body>
</html>