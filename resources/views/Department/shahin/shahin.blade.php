<!DOCTYPE html>
<html>
<head>
	<title>Department</title>
</head>
<body>

	<h2>Department Create</h2>   <h3> <a href="/departments"> View Department</a> </h3>
 
 @if ($errors->any())
     @foreach ($errors->all() as $error)
         <div>{{$error}}</div>
     @endforeach
 @endif
 
@if(session()->has('success'))
    <div class="alert alert-success" style="color: green;font-weight: bold;">
        {{ session()->get('success') }}
    </div>
@endif

	<form action="/departments" method="POST">
		{{csrf_field()}}

		<input type="text" name="dpt_name" placeholder="department name">
		<input type="text" name="dpt_code" placeholder="department code">
		<input type="submit" value="Save">
		
	</form>

<br>
<br>
<br>
	<table border="1">
		
		<tr>
			<th>Sl</th>
			<th>Dpt Name</th>
			<th>Dpt Code</th>
			<th>Action</th>
			<th>Delete</th>
			 
		</tr>
  

       <?php $i= $departments->perPage()*($departments->currentPage()-1); ?>

		@foreach($departments as $key=>$data)

		<tr>
			<td>  {{++$i}} </td>
			<td>{{$data->dpt_name}}</td>
			<td>{{$data->dpt_code}}</td>
			<td><a href="/departments/{{$data->id}}/edit">Edit </a>|   </td>

            <td>
			{!! Form::open(['url' => '/departments/'.$data->id, 'method'=>'Delete']) !!}

			<button type="submit" class="btn btn-warning"  onclick="return confirm('are you sure?')">Delete</button>
   
            {!! Form::close() !!}

        </td>
		</tr>

		@endforeach




	</table>

	{{$departments->links()}}

</body>
</html>